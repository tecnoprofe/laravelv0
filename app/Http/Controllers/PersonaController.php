<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Persona;


class PersonaController extends Controller
{

    public function index()
    {

        $per=Persona::all(); //select * from persona
        return view('persona.index',compact('per'));
    }

    
    public function create()
    {
        return view('persona.create');
    }

   
    public function store(Request $request)
    {        
        $x=Persona::create($request->all());        
    }

   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

   
    public function update(Request $request, $id)
    {
        //
    }
   
    public function destroy($id)
    {        
        $x=Persona::find($id);   //select * from persona where id=$id
        $x->delete();
        return redirect()->route('persona.index');
    }
        
    public function saludoespecial(){        
        return "hola";
    }
}
